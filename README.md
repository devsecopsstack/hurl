# GitLab CI template for Hurl

This project implements a GitLab CI/CD template to run your automated tests with [Hurl](https://hurl.dev/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/hurl/gitlab-ci-hurl@1.3.0
    # 2: set/override component inputs
    inputs:
      review-enabled: true # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/hurl'
    ref: '1.3.0'
    file: '/templates/gitlab-ci-hurl.yml'

variables:
  # 2: set/override template variables
  REVIEW_ENABLED: "true" # ⚠ this is only an example
```

## `hurl` job

This job starts [Hurl](https://hurl.dev/) (functional) tests.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `image` / `HURL_IMAGE` | The Docker image used to run Hurl. | `ghcr.io/orange-opensource/hurl:latest` |
| `test-files` / `HURL_TEST_FILES` | The Hurl test files to run | `hurl/*.hurl` |
| `extra-args` / `HURL_EXTRA_ARGS` | Hurl extra [run options](https://hurl.dev/docs/manual.html#options) | _none_ |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Hurl tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/hurl.xunit.xml` | [xUnit](https://hurl.dev/docs/manual.html#report-junit) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### base url auto evaluation

By default, the Hurl template tries to auto-evaluate the base server url
(i.e. the variable pointing at server under test) by looking either for an `$environment_url` variable or for an 
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url` 
or through a basic `environment_url.txt` file, then the Hurl test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Hurl tests.

When successfully determined, the base url is then [injected as `base_url` variable](https://hurl.dev/docs/templates.html#injecting-variables), and can be freely used in your Hurl scripts with the `{{base_url}}` expression.

### Hook scripts

The Hurl template supports _optional_ **hook scripts** from your project, located in the root directory to perform additional project-specific logic:

* `pre-hurl.sh` is executed **before** running Hurl,
* `post-hurl.sh` is executed **after** running Hurl (whichever the tests status).
