# [1.3.0](https://gitlab.com/to-be-continuous/hurl/compare/1.2.1...1.3.0) (2024-05-06)


### Features

* add hook scripts support ([dad589d](https://gitlab.com/to-be-continuous/hurl/commit/dad589da6183afebb7ac1716d70e6e0cdb09e24b))

## [1.2.1](https://gitlab.com/to-be-continuous/hurl/compare/1.2.0...1.2.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([7ac5754](https://gitlab.com/to-be-continuous/hurl/commit/7ac5754c07fe161eb556d231968c52cb3abc6f30))

# [1.2.0](https://gitlab.com/to-be-continuous/hurl/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([bc5211a](https://gitlab.com/to-be-continuous/hurl/commit/bc5211a23b79f4d2bc16d6cb28ae4960eab34e3c))

# [1.1.0](https://gitlab.com/to-be-continuous/hurl/compare/1.0.1...1.1.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([38a0fee](https://gitlab.com/to-be-continuous/hurl/commit/38a0feea88eda267071494d8f0bf5a45bb539858))

## [1.0.1](https://gitlab.com/to-be-continuous/hurl/compare/1.0.0...1.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([ffa2590](https://gitlab.com/to-be-continuous/hurl/commit/ffa259025e819895d2f9bf3ee7285bd004fb0892))
